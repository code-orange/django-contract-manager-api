from django.forms.models import model_to_dict
from tastypie.exceptions import BadRequest
from tastypie.resources import Resource
from tastypie.serializers import Serializer

from django_contract_manager_microsoft.django_contract_manager_microsoft.func import (
    get_tenant_id_by_domain,
)
from django_contract_manager_models.django_contract_manager_models.models import *
from django_tastypie_generalized_api_auth.django_tastypie_generalized_api_auth.authentification import (
    ApiAuthCustomerAuthentication,
)


class ContractBaseResource(Resource):
    class Meta:
        always_return_data = True
        allowed_methods = ["get"]
        serializer = Serializer()
        authentication = ApiAuthCustomerAuthentication()

    def get_resource_uri(self, bundle_or_obj=None, url_name="api_dispatch_list"):
        return


class ContractResource(ContractBaseResource):
    class Meta(ContractBaseResource.Meta):
        allowed_methods = ["get", "post"]


class TenantResource(ContractBaseResource):
    class Meta(ContractBaseResource.Meta):
        allowed_methods = ["get"]
        authentication = ApiAuthCustomerAuthentication()

    def obj_get(self, bundle, **kwargs):
        customer_id = kwargs["pk"]

        if not bundle.request.user.id == int(customer_id):
            if not bundle.request.user.auth_api_key.super_admin == True:
                raise BadRequest("Wrong tenant.")

        bundle.obj = ContractCustSettings.objects.get(customer__id=int(customer_id))

        return bundle

    def obj_get_list(self, bundle, **kwargs):
        bundle.obj = list()

        return bundle

    def dehydrate(self, bundle):
        if bundle.request.method == "GET":
            bundle.data = model_to_dict(bundle.obj.obj)

            if "ms_tenant_domain" in bundle.data:
                if bundle.data["ms_tenant_domain"]:
                    bundle.data["ms_tenant_id"] = get_tenant_id_by_domain(
                        bundle.data["ms_tenant_domain"]
                    )

        return bundle
