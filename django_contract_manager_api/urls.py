from django.urls import path, include
from tastypie.api import Api

from django_contract_manager_api.django_contract_manager_api.api.resources import *
from django_tastypie_generalized_api_auth.django_tastypie_generalized_api_auth.api.resources import *

v1_api = Api(api_name="v1")

# API GENERAL
v1_api.register(ApiAuthCustomerApiKeyResource())
v1_api.register(ApiAuthCustomerResource())

# API Contracts
v1_api.register(ContractResource())
v1_api.register(TenantResource())

urlpatterns = [
    path("", include(v1_api.urls)),
]
