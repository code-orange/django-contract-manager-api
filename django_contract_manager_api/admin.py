from django.apps import apps
from django.contrib import admin

for model in apps.get_app_config("django_contract_manager_api").models.values():
    admin.site.register(model)
