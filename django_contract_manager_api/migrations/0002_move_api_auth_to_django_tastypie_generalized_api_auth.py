# Generated by Django 3.2.14 on 2022-07-06 15:21

from django.db import migrations


def copy_api_key(apps, schema_editor):
    CustomerApiKey = apps.get_model("django_contract_manager_api", "CustomerApiKey")
    ApiAuthCustomerApiKey = apps.get_model(
        "django_tastypie_generalized_api_auth", "ApiAuthCustomerApiKey"
    )

    for api_key in CustomerApiKey.objects.all():
        ApiAuthCustomerApiKey.objects.create(
            user_id=api_key.user_id,
            super_admin=api_key.super_admin,
            key=api_key.key,
            created=api_key.created,
        )


def clear_api_key(apps, schema_editor):
    ApiAuthCustomerApiKey = apps.get_model(
        "django_tastypie_generalized_api_auth", "ApiAuthCustomerApiKey"
    )
    ApiAuthCustomerApiKey.objects.all().delete()


class Migration(migrations.Migration):
    dependencies = [
        ("django_contract_manager_api", "0001_initial"),
        ("django_tastypie_generalized_api_auth", "0002_use_timezone_aware_timestamp"),
    ]

    operations = [
        migrations.RunPython(copy_api_key, clear_api_key),
    ]
